start config.xlaunch

docker build . -t ceti-panda-gazebo-workspace

docker run -it^
 --env="DISPLAY=host.docker.internal:0"^
 --user="ros"^
 ceti-panda-gazebo-workspace^
 %*

pause