#!/bin/sh

docker build . -t ceti-panda-gazebo-workspace

XSOCK=/tmp/.X11-unix

/usr/X11/bin/xhost + 127.0.0.1
defaults write org.macosforge.xquartz.X11 enable_iglx -bool true

docker run -it \
    --volume=$XSOCK:$XSOCK:rw \
    --env="LIBGL_ALWAYS_INDIRECT=1" \
    --env="DISPLAY=host.docker.internal:0" \
    --user="ros" \
    ceti-panda-gazebo-workspace \
    "$@"