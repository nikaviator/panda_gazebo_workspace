#!/bin/bash

# http://wiki.ros.org/docker/Tutorials/GUI

docker build . -t ceti-panda-gazebo-workspace \
    --build-arg UID=$UID \
    --build-arg GID=$GID

XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth
touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -

docker run -it \
    --volume=$XSOCK:$XSOCK:rw \
    --volume=$XAUTH:$XAUTH:rw \
    --env="XAUTHORITY=${XAUTH}" \
    --env="DISPLAY" \
    --device=/dev/dri \
    --group-add video \
    --user="ros" \
    ceti-panda-gazebo-workspace \
    "$@"